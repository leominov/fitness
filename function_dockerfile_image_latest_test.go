package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDockerfileImageLatestFunction_Run(t *testing.T) {
	f := &DockerfileImageLatestFunction{}
	errs := f.run("testdata/dockerfile-image-latest/not-found.Dockerfile")
	assert.Zero(t, len(errs))

	errs = f.run("testdata/dockerfile-image-latest/valid.Dockerfile")
	assert.Zero(t, len(errs))

	errs = f.run("testdata/dockerfile-image-latest/valid-base.Dockerfile")
	assert.Zero(t, len(errs))

	errs = f.run("testdata/dockerfile-image-latest/invalid-no-tag.Dockerfile")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "node")
	}

	errs = f.run("testdata/dockerfile-image-latest/invalid-latest.Dockerfile")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "node:latest")
	}
}
