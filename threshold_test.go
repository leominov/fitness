package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestThreshold_Parse(t *testing.T) {
	th := &Threshold{}
	assert.NoError(t, th.Parse())
	th = &Threshold{
		ErrorAfter: "ABCD",
	}
	assert.Error(t, th.Parse())
	th = &Threshold{
		WarningAfter: "ABCD",
	}
	assert.Error(t, th.Parse())
	th = &Threshold{
		ErrorAfter:   "2021-01-01",
		WarningAfter: "2021-01-01",
	}
	assert.NoError(t, th.Parse())
}

func TestThreshold_String(t *testing.T) {
	th := &Threshold{}
	assert.Empty(t, th.String())
	assert.True(t, th.IsEmpty())
	th = &Threshold{
		ErrorAfter:   "2021-01-01",
		WarningAfter: "2021-01-01",
	}
	assert.NotEmpty(t, th.String())
	assert.False(t, th.IsEmpty())
}

func TestThreshold_IsError(t *testing.T) {
	th := &Threshold{}
	_ = th.Parse()
	assert.False(t, th.IsError())
	th.ErrorAfter = time.Now().Add(-24 * time.Hour).Format("2006-01-02")
	_ = th.Parse()
	assert.True(t, th.IsError())
	th.ErrorAfter = time.Now().Add(24 * time.Hour).Format("2006-01-02")
	_ = th.Parse()
	assert.False(t, th.IsError())
}

func TestThreshold_IsWarn(t *testing.T) {
	th := &Threshold{}
	_ = th.Parse()
	assert.False(t, th.IsWarn())
	th.WarningAfter = time.Now().Add(-24 * time.Hour).Format("2006-01-02")
	_ = th.Parse()
	assert.True(t, th.IsWarn())
	th.WarningAfter = time.Now().Add(24 * time.Hour).Format("2006-01-02")
	_ = th.Parse()
	assert.False(t, th.IsWarn())
}
