package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParsePackagelock(t *testing.T) {
	_, err := ParsePackagelock("testdata/packagelock/valid.json")
	assert.NoError(t, err)
	_, err = ParsePackagelock("testdata/packagelock/not-found.json")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "no such file")
	}
	_, err = ParsePackagelock("testdata/packagelock/invalid.json")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "invalid")
	}
}
