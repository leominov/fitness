package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadFromFile(t *testing.T) {
	_, err := LoadFromFile("testdata/config/valid.yaml")
	assert.NoError(t, err)
	_, err = LoadFromFile("testdata/config/not-found.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "no such file")
	}
	_, err = LoadFromFile("testdata/config/invalid-format.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "yaml")
	}
	_, err = LoadFromFile("testdata/config/invalid-threshold.yaml")
	assert.Error(t, err)
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "cannot parse")
	}
}

func TestConfig_EnabledFunctions(t *testing.T) {
	config, err := LoadFromFile("testdata/config/valid.yaml")
	if !assert.NoError(t, err) {
		return
	}
	assert.Equal(t, 1, len(config.EnabledFunctions()))
}

func TestConfig_String(t *testing.T) {
	config, err := LoadFromFile("testdata/config/valid.yaml")
	if !assert.NoError(t, err) {
		return
	}
	assert.NotEmpty(t, config.String())
}
