package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadFunctionsFromDirectory(t *testing.T) {
	functions := len(FunctionsRegistry)
	LoadFunctionsFromDirectory("not-found")
	assert.Equal(t, functions, len(FunctionsRegistry))
	LoadFunctionsFromDirectory("fitness.d")
	assert.Greater(t, len(FunctionsRegistry), functions)
}

func TestNewExecFunction(t *testing.T) {
	a := NewExecFunction("whoami", "")
	assert.True(t, a.Run(nil) == nil)
	b := NewExecFunction("not-found-binary", "")
	assert.True(t, b.Run(nil) != nil)
}

func TestNewErrFunctionFailed(t *testing.T) {
	err := errors.New("foobar")
	assert.False(t, IsErrFunctionFailed(err))

	err = NewErrFunctionFailed(errors.New("foobar"))
	assert.NotEmpty(t, err.Error())
	assert.True(t, IsErrFunctionFailed(err))

	err = ErrFunctionFailed{
		Message: "foobar",
		Docs:    "https://google.com",
	}
	assert.NotEmpty(t, err.Error())
	assert.True(t, IsErrFunctionFailed(err))
}
