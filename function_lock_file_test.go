package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLockFileFunction_Run(t *testing.T) {
	l := &LockFileFunction{}
	errs := l.run("testdata/lock-file/valid-empty")
	assert.Nil(t, errs)
	errs = l.run("testdata/lock-file/valid-lock")
	assert.Nil(t, errs)
	errs = l.run("testdata/lock-file/invalid")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "lock-file")
	}
	errs = l.run("testdata/lock-file/invalid-nested")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "lock-file")
	}
}
