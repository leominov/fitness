package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReporter_Send(t *testing.T) {
	var requestMap = make(map[string]int)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reqID := r.Method + ":" + r.URL.Path
		if _, ok := requestMap[reqID]; !ok {
			requestMap[reqID] = 0
		}
		requestMap[reqID]++
		w.WriteHeader(http.StatusOK)
	}))
	defer ts.Close()

	r := NewReporter(ts.URL, "foobar")
	r.SetStatus("function1", SuccessFunctionStatus)
	r.Send()
	_, ok := requestMap["PUT:/metrics/job/fitness/instance/gitlab-runner/project_path/foobar"]
	assert.True(t, ok)

	r = NewReporter(ts.URL, "")
	r.SetStatus("function1", SuccessFunctionStatus)
	r.Send()
	_, ok = requestMap["PUT:/metrics/job/fitness/instance/gitlab-runner/project_path/test"]
	assert.True(t, ok)
}
