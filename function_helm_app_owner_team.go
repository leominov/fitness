package main

import (
	"fmt"
	"os"
	"path"

	"gopkg.in/yaml.v2"
)

type HelmAppOwnerTeamFunction struct {
	docs string
}

type values struct {
	AppTemplate struct {
		App struct {
			OwnerTeam string `yaml:"owner_team"`
		} `yaml:"app"`
	} `yaml:"app-template"`
}

func init() {
	f := &HelmAppOwnerTeamFunction{
		docs: "https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/3291709455/helm-app-owner-team",
	}
	RegisterFunction("helm-app-owner-team", f)
}

func (l *HelmAppOwnerTeamFunction) Run(_ map[string]interface{}) []error {
	return l.run("helm")
}

func (l *HelmAppOwnerTeamFunction) DocsLink() string {
	return l.docs
}

func (l *HelmAppOwnerTeamFunction) run(workDir string) (errs []error) {
	_, err := os.Stat(workDir)
	if err != nil {
		return nil
	}

	commonValues := path.Join(workDir, "values.yaml")
	contains, _ := IsValuesFileContainsOwnerTeam(commonValues)
	// Если файл values.yaml существует, то от него будет наследоваться
	// структура для публикации во все остальные окружения, поэтому если
	// там задан owner_team, то в остальных файлах указывать дополнительно
	// не требуется. Соответственно, если не указано, то owner_team должен
	// быть задан в каждом отдельном файле.
	if contains {
		return nil
	}

	var containsEnv bool
	for _, filename := range []string{
		path.Join(workDir, "preprod.values.yaml"),
		path.Join(workDir, "prod.values.yaml"),
		path.Join(workDir, "stage.values.yaml"),
	} {
		_, err := os.Stat(filename)
		if err != nil {
			continue
		}
		contains, err := IsValuesFileContainsOwnerTeam(filename)
		if err != nil {
			errs = append(errs, NewErrFunctionFailed(err))
			return errs
		}
		if !contains {
			err = fmt.Errorf("missed app-template.app.owner_team in %s or %s file", filename, commonValues)
			errs = append(errs, NewErrFunctionFailed(err))
		} else if !containsEnv {
			containsEnv = true
		}
	}

	if len(errs) == 0 && !contains && !containsEnv {
		err = fmt.Errorf("missed app-template.app.owner_team in %s file", commonValues)
		errs = append(errs, NewErrFunctionFailed(err))
	}

	return errs
}

func IsValuesFileContainsOwnerTeam(filename string) (bool, error) {
	b, err := os.ReadFile(filename)
	if err != nil {
		return false, err
	}
	val := &values{}
	err = yaml.Unmarshal(b, val)
	if err != nil {
		return false, err
	}
	if len(val.AppTemplate.App.OwnerTeam) == 0 {
		return false, nil
	}
	return true, nil
}
