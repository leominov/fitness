package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseGoMod(t *testing.T) {
	_, err := ParseGoMod("testdata/gomod/not-found.go.mod")
	assert.Error(t, err)

	gomod, err := ParseGoMod("testdata/gomod/internal.go.mod")
	if assert.NoError(t, err) {
		assert.True(t, gomod.IsInternalPackage())
	}

	gomod, err = ParseGoMod("testdata/gomod/public.go.mod")
	if assert.NoError(t, err) {
		assert.False(t, gomod.IsInternalPackage())
	}
}
