package main

import (
	"strings"
	"time"
)

type Threshold struct {
	ErrorAfter   string `yaml:"errorAfter,omitempty"`
	WarningAfter string `yaml:"warningAfter,omitempty"`

	errorAfter   time.Time
	warningAfter time.Time
}

func (t *Threshold) Parse() error {
	if len(t.ErrorAfter) > 0 {
		tp, err := time.Parse("2006-01-02", t.ErrorAfter)
		if err != nil {
			return err
		}
		t.errorAfter = tp
	}
	if len(t.WarningAfter) > 0 {
		tp, err := time.Parse("2006-01-02", t.WarningAfter)
		if err != nil {
			return err
		}
		t.warningAfter = tp
	}
	return nil
}

func (t *Threshold) String() string {
	var result []string
	if len(t.WarningAfter) > 0 {
		result = append(result, "warning after: "+t.WarningAfter)
	}
	if len(t.ErrorAfter) > 0 {
		result = append(result, "error after: "+t.ErrorAfter)
	}
	return strings.Join(result, ", ")
}

func (t *Threshold) IsEmpty() bool {
	return len(t.String()) == 0
}

func (t *Threshold) IsError() bool {
	return len(t.ErrorAfter) > 0 && time.Now().After(t.errorAfter)
}

func (t *Threshold) IsWarn() bool {
	return len(t.WarningAfter) > 0 && time.Now().After(t.warningAfter)
}
