# Fitness

Фитнес-функции для форсирования изменений со стороны разработки.

## Функции

* `dockerfile-image-latest` – использование образов с привязкой к версии;
* `gemfile-access-token` – отсутствие Gitlab Access Token в `Gemfile.lock`;
* `go-logger` – наличие пакета `gitlab.qleanlabs.ru/platform/go-libs/logger` в зависимостях;
* `lock-file` – наличие lock-файла (например, `go.sum` или `package-lock.json`);
* `node-nestjs-logger` – использование библиотеки `@qlean/nestjs-logger` актуальной версии;
* `readme` – наличие файла `README.md`;
* `helm-app-owner-team` - наличие `owner_team` в Helm Values.

## Настройки

Содержимое файла `.fitness.default.yaml` является базовым и поставляется вместе с образом. Наличие файла `.fitness.yaml` в корне проверяемого репозитория переопределяет базовые настройки (можно отключить ненужную функцию или задать новые даты действия).

## Ссылки

* https://grafana.infra.cloud.qlean.ru/d/6idLLYHnk/fitness?orgId=1
