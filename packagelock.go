package main

import (
	"encoding/json"
	"io/ioutil"
	"strings"
)

type Packagelock struct {
	Name         string                            `json:"name"`
	Version      string                            `json:"version"`
	Dependencies map[string]*PackagelockDependency `json:"dependencies"`
}

type PackagelockDependency struct {
	Version string `json:"version"`
	Dev     bool   `json:"dev"`
}

func ParsePackagelock(filename string) (*Packagelock, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	lock := &Packagelock{}
	err = json.Unmarshal(b, lock)
	if err != nil {
		return nil, err
	}
	return lock, nil
}

func (p *Packagelock) IsInternalPackage() bool {
	return strings.HasPrefix(p.Name, "@")
}
