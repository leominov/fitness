package main

import (
	"fmt"
	"os"

	"github.com/Masterminds/semver"
)

const (
	nodeLogger           = "@qlean/nestjs-logger"
	nodeLoggerMinVersion = "0.4.4"
)

type NodeNestJSLoggerFunction struct {
	docs string
}

func init() {
	f := &NodeNestJSLoggerFunction{
		docs: "https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/3291709462/node-nestjs-logger",
	}
	RegisterFunction("node-nestjs-logger", f)
}

func (n *NodeNestJSLoggerFunction) run(filename string) []error {
	if _, err := os.Stat(filename); err != nil {
		return nil
	}

	lock, err := ParsePackagelock(filename)
	if err != nil {
		return []error{NewErrFunctionFailed(err)}
	}

	// Нет необходимости проверять зависимости у собственных библиотек
	if lock.IsInternalPackage() {
		return nil
	}

	info, ok := lock.Dependencies[nodeLogger]
	if !ok {
		err := fmt.Errorf("package %s not found", nodeLogger)
		return []error{NewErrFunctionFailed(err)}
	}

	v, err := semver.NewVersion(info.Version)
	if err != nil {
		return []error{NewErrFunctionFailed(err)}
	}

	c, _ := semver.NewConstraint(fmt.Sprintf(">= %s", nodeLoggerMinVersion))
	if ok, _ := c.Validate(v); !ok {
		err := fmt.Errorf("package %s version must be greater or equal to %s",
			nodeLogger, nodeLoggerMinVersion)
		return []error{NewErrFunctionFailed(err)}
	}
	return nil
}

func (n *NodeNestJSLoggerFunction) Run(_ map[string]interface{}) []error {
	// В некоторых приложениях серверный код лежит в отдельном каталоге
	if _, err := os.Stat("server/package-lock.json"); err == nil {
		return n.run("server/package-lock.json")
	}
	return n.run("package-lock.json")
}

func (n *NodeNestJSLoggerFunction) DocsLink() string {
	return n.docs
}
