package main

import (
	"io"
	"os"

	"github.com/moby/buildkit/frontend/dockerfile/parser"
)

type Command struct {
	Cmd       string
	SubCmd    string
	Json      bool
	Original  string
	StartLine int
	EndLine   int
	Flags     []string
	Value     []string
}

type IOError struct {
	Msg string
}

func (e IOError) Error() string {
	return e.Msg
}

type ParseError struct {
	Msg string
}

func (e ParseError) Error() string {
	return e.Msg
}

func ParseReader(file io.Reader) ([]Command, error) {
	res, err := parser.Parse(file)
	if err != nil {
		return nil, ParseError{err.Error()}
	}

	var ret []Command
	for _, child := range res.AST.Children {
		cmd := Command{
			Cmd:       child.Value,
			Original:  child.Original,
			StartLine: child.StartLine,
			EndLine:   child.EndLine,
			Flags:     child.Flags,
		}

		// Only happens for ONBUILD
		if child.Next != nil && len(child.Next.Children) > 0 {
			cmd.SubCmd = child.Next.Children[0].Value
			child = child.Next.Children[0]
		}

		cmd.Json = child.Attributes["json"]
		for n := child.Next; n != nil; n = n.Next {
			cmd.Value = append(cmd.Value, n.Value)
		}

		ret = append(ret, cmd)
	}
	return ret, nil
}

func ParseDockerfile(filename string) ([]Command, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, IOError{err.Error()}
	}
	defer file.Close()

	return ParseReader(file)
}
