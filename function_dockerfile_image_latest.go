package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

type DockerfileImageLatestFunction struct {
	docs string
}

var (
	deniedDockerfileImagesRefs = []*regexp.Regexp{
		regexp.MustCompile(`.+:alpine$`),
		regexp.MustCompile(`.+:master$`),
		regexp.MustCompile(`.+:latest$`),
		regexp.MustCompile(`.+:debug$`),
	}
)

func init() {
	f := &DockerfileImageLatestFunction{
		docs: "https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/3291480065/dockerfile-image-latest",
	}
	RegisterFunction("dockerfile-image-latest", f)
}

func (d *DockerfileImageLatestFunction) run(filename string) []error {
	if _, err := os.Stat(filename); err != nil {
		return nil
	}

	commands, err := ParseDockerfile(filename)
	if err != nil {
		return []error{NewErrFunctionFailed(err)}
	}

	var (
		// Список алиасов нужен, чтобы разделить между собой ссылки от
		// полных имен образов, так как без этого алиас будет подхожить
		// под образ без какого-либо тега
		aliases = make(map[string]struct{})
		images  []string
	)
	for _, cmd := range commands {
		if strings.ToLower(cmd.Cmd) != "from" || len(cmd.Value) == 0 {
			continue
		}
		images = append(images, cmd.Value[0])
		// FROM ubuntu:16 AS base
		if len(cmd.Value) == 3 && strings.ToLower(cmd.Value[1]) == "as" {
			aliases[cmd.Value[2]] = struct{}{}
		}
	}

	var errs []error
	for _, val := range images {
		if _, ok := aliases[val]; ok {
			continue
		}
		if !strings.Contains(val, ":") {
			err := fmt.Errorf("found image without tag: %s", val)
			errs = append(errs, NewErrFunctionFailed(err))
			continue
		}
		for _, re := range deniedDockerfileImagesRefs {
			if !re.MatchString(val) {
				continue
			}
			err := fmt.Errorf("found image with latest tag: %s", val)
			errs = append(errs, NewErrFunctionFailed(err))
			break
		}
	}

	return errs
}

func (d *DockerfileImageLatestFunction) Run(_ map[string]interface{}) []error {
	return d.run("Dockerfile")
}

func (d *DockerfileImageLatestFunction) DocsLink() string {
	return d.docs
}
