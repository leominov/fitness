#!/usr/bin/env bash

docker run --rm --name=pgw -p 9091:9091 --detach prom/pushgateway:v1.4.1
echo "export PUSHGATEWAY_URL=http://127.0.0.1:9091"
echo "To stop: docker stop pgw"
