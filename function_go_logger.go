package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

const (
	goLogger = "gitlab.qleanlabs.ru/platform/go-libs/logger"
)

type GoLoggerFunction struct {
	docs string
}

func init() {
	f := &GoLoggerFunction{
		docs: "https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/3291709448/go-logger",
	}
	RegisterFunction("go-logger", f)
}

func (g *GoLoggerFunction) run(filename string) []error {
	if _, err := os.Stat(filename); err != nil {
		return nil
	}
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return []error{NewErrFunctionFailed(err)}
	}
	if !strings.Contains(string(b), goLogger) {
		err := fmt.Errorf("%s not found in go.sum", goLogger)
		return []error{NewErrFunctionFailed(err)}
	}
	return nil
}

func (g *GoLoggerFunction) Run(_ map[string]interface{}) []error {
	gomod, err := ParseGoMod("go.mod")
	if err != nil {
		return nil
	}
	// Не проверяем, если это не репозиторий сервиса
	if gomod.IsInternalPackage() {
		return nil
	}
	return g.run("go.sum")
}

func (g *GoLoggerFunction) DocsLink() string {
	return g.docs
}
