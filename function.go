package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
)

var FunctionsRegistry = make(map[string]Function)

type Function interface {
	Run(meta map[string]interface{}) []error
	DocsLink() string
}

func RegisterFunction(name string, f Function) {
	FunctionsRegistry[name] = f
}

func LoadFunctionsFromDirectory(dirname string) {
	items, err := ioutil.ReadDir(dirname)
	if err != nil {
		return
	}
	for _, item := range items {
		if item.IsDir() || item.Mode()&0111 != 0111 {
			continue
		}
		f := NewExecFunction(path.Join(dirname, item.Name()), "")
		RegisterFunction(item.Name(), f)
	}
}

type ExecFunction struct {
	bin  string
	docs string
}

func NewExecFunction(bin, docs string) *ExecFunction {
	return &ExecFunction{
		bin:  bin,
		docs: docs,
	}
}

func (e *ExecFunction) Run(_ map[string]interface{}) []error {
	cmd := exec.Command(e.bin)
	cmd.Env = os.Environ()
	b, err := cmd.CombinedOutput()
	if err != nil {
		return []error{fmt.Errorf("%v: %s", err, string(b))}
	}
	return nil
}

func (e *ExecFunction) DocsLink() string {
	return e.docs
}

func (e *ExecFunction) DefaultConfig() *FunctionConfig {
	return &FunctionConfig{}
}

type ErrFunctionFailed struct {
	Message string
	Docs    string
}

func IsErrFunctionFailed(err error) bool {
	_, ok := err.(ErrFunctionFailed)
	return ok
}

func (e ErrFunctionFailed) Error() string {
	if len(e.Docs) == 0 {
		return e.Message
	}
	return fmt.Sprintf("%s (details: %s)", e.Message, e.Docs)
}

func NewErrFunctionFailed(err error) ErrFunctionFailed {
	return ErrFunctionFailed{
		Message: err.Error(),
	}
}
