package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseDockerfile(t *testing.T) {
	_, err := ParseDockerfile("not-found.Dockerfile")
	assert.Error(t, err)
	_, err = ParseDockerfile("testdata/dockerfile/valid.Dockerfile")
	assert.NoError(t, err)
	_, err = ParseDockerfile("testdata/dockerfile/invalid.Dockerfile")
	assert.Error(t, err)
}
