package main

import (
	"fmt"
	"os"
	"regexp"
)

var (
	gemfileAccessTokenRE = regexp.MustCompile(`https://.+:.+@gitlab.qleanlabs.ru`)
)

type GemfileAccessTokenFunction struct {
	docs string
}

func init() {
	f := &GemfileAccessTokenFunction{
		docs: "https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/3291709441/gemfile-access-token",
	}
	RegisterFunction("gemfile-access-token", f)
}

func (g *GemfileAccessTokenFunction) run(filename string) []error {
	b, err := os.ReadFile(filename)
	if err != nil {
		return nil
	}
	if gemfileAccessTokenRE.Match(b) {
		err := fmt.Errorf("access token found in %s file", filename)
		return []error{NewErrFunctionFailed(err)}
	}
	return nil
}

func (g *GemfileAccessTokenFunction) Run(_ map[string]interface{}) []error {
	return g.run("Gemfile.lock")
}

func (g *GemfileAccessTokenFunction) DocsLink() string {
	return g.docs
}
