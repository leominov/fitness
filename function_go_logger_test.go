package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGoLoggerFunction_Run(t *testing.T) {
	f := &GoLoggerFunction{}
	errs := f.run("testdata/go-logger/not-found.go.sum")
	assert.Zero(t, len(errs))

	errs = f.run("testdata/go-logger/valid.go.sum")
	assert.Zero(t, len(errs))

	errs = f.run("testdata/go-logger/invalid.go.sum")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "not found")
	}
}
