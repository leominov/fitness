package main

import (
	"io/ioutil"
	"regexp"
)

var (
	// Example: module gitlab.qleanlabs.ru/platform/go-libs/service-loyalty-bonus-sdk
	internalModuleRegExp = regexp.MustCompile(`^module gitlab.qleanlabs.ru/platform/go-libs`)
)

type GoMod struct {
	internal bool
}

func ParseGoMod(filename string) (*GoMod, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	g := &GoMod{}
	g.internal = internalModuleRegExp.Match(b)
	return g, nil
}

func (g *GoMod) IsInternalPackage() bool {
	return g.internal
}
