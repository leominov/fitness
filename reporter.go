package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
)

const (
	SuccessFunctionStatus = float64(0)
	WarningFunctionStatus = float64(1)
	ErrorFunctionStatus   = float64(2)
)

type Reporter struct {
	pusher               *push.Pusher
	functionStatusMetric *prometheus.GaugeVec
}

func NewReporter(addr, projectPath string) *Reporter {
	if projectPath == "" {
		projectPath = "test"
	}

	reporter := &Reporter{
		functionStatusMetric: prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Namespace: "fitness",
			Subsystem: "function",
			Name:      "status",
			Help:      "Status of fitness function (0 - success, 1 - warning, 2 - error).",
		}, []string{"name"}),
	}

	pusher := push.New(addr, "fitness")
	pusher.Grouping("instance", "gitlab-runner")
	pusher.Grouping("project_path", projectPath)
	pusher.Collector(reporter.functionStatusMetric)
	reporter.pusher = pusher

	return reporter
}

func (r *Reporter) SetStatus(name string, status float64) {
	r.functionStatusMetric.WithLabelValues(name).Set(status)
}

func (r *Reporter) Send() {
	_ = r.pusher.Push()
}
