package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNpmrcAccessTokenFunction_Run(t *testing.T) {
	n := &NpmrcAccessTokenFunction{}
	errs := n.run([]string{"testdata/npmrc-access-token/valid-1"})
	assert.Nil(t, errs)
	errs = n.run([]string{"testdata/npmrc-access-token/valid-2"})
	assert.Nil(t, errs)
	errs = n.run([]string{"testdata/npmrc-access-token/not-found"})
	assert.Nil(t, errs)
	errs = n.run([]string{"testdata/npmrc-access-token/invalid"})
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "access token")
	}
}
