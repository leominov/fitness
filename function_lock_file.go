package main

import (
	"fmt"
	"os"
	"path"
)

type LockFileFunction struct {
	docs string
}

var (
	packageToLockFilesMap = map[string][]string{
		"Gemfile":             {"Gemfile.lock"},                                 // Ruby
		"Gopkg.toml":          {"Gopkg.lock"},                                   // Go
		"go.mod":              {"go.sum"},                                       // Go
		"package.json":        {"package-lock.json", "yarn.lock"},               // Node
		"server/package.json": {"server/package-lock.json", "server/yarn.lock"}, // Node
		"web/package.json":    {"web/package-lock.json", "web/yarn.lock"},       // Node
		"mix.exs":             {"mix.lock"},                                     // Elixir
		"composer.json":       {"composer.lock"},                                // PHP
	}
)

func init() {
	f := &LockFileFunction{
		docs: "https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/3291414555/lock-file",
	}
	RegisterFunction("lock-file", f)
}

func (l *LockFileFunction) Run(_ map[string]interface{}) []error {
	return l.run("")
}

func (l *LockFileFunction) DocsLink() string {
	return l.docs
}

func (l *LockFileFunction) run(workDir string) []error {
	var errs []error
	for packageFile, lockFiles := range packageToLockFilesMap {
		if _, err := os.Stat(path.Join(workDir, packageFile)); err != nil {
			continue
		}
		var lockFileFound bool
		for _, lockFile := range lockFiles {
			if _, err := os.Stat(path.Join(workDir, lockFile)); err == nil {
				lockFileFound = true
				break
			}
		}
		if lockFileFound {
			continue
		}
		err := fmt.Errorf("repository contains %s file, but lock-file not found", packageFile)
		errs = append(errs, NewErrFunctionFailed(err))
	}
	return errs
}
