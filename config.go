package main

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

var config = NewConfig()

type Config struct {
	Functions map[string]*FunctionConfig `yaml:"functions"`
}

type FunctionConfig struct {
	Enabled   *bool                  `yaml:"enabled,omitempty"`
	Threshold Threshold              `yaml:"thresholds,omitempty"`
	Meta      map[string]interface{} `yaml:"meta,omitempty"`
}

func NewConfig() *Config {
	c := &Config{}
	c.Functions = make(map[string]*FunctionConfig)
	for name := range FunctionsRegistry {
		c.Functions[name] = &FunctionConfig{
			Enabled: Bool(true),
		}
	}
	return c
}

func LoadFromFile(filename string) (*Config, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	c := &Config{}
	err = yaml.Unmarshal(b, c)
	if err != nil {
		return nil, err
	}

	c.setDefaults()

	err = c.parseThresholds()
	if err != nil {
		return nil, err
	}

	return c, nil
}

func (c *Config) parseThresholds() error {
	for _, f := range c.Functions {
		err := f.Threshold.Parse()
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Config) setDefaults() {
	for name, f := range c.Functions {
		if f == nil {
			f = &FunctionConfig{}
			c.Functions[name] = f
		}
		if f.Enabled == nil {
			f.Enabled = Bool(true)
		}
	}
}

func (c *Config) Merge(conf *Config) {
	for name, f := range conf.Functions {
		if _, ok := FunctionsRegistry[name]; !ok {
			continue
		}
		c.Functions[name] = f
	}
}

func (c *Config) String() string {
	b, _ := yaml.Marshal(c)
	return string(b)
}

func (c *Config) EnabledFunctions() []string {
	var result []string
	for name, f := range c.Functions {
		if !f.IsEnabled() {
			continue
		}
		result = append(result, name)
	}
	return result
}

func (c *Config) SetFunctionConfig(name string, config *FunctionConfig) {
	c.Functions[name] = config
}

func (f *FunctionConfig) IsEnabled() bool {
	return f.Enabled == nil || *f.Enabled == true
}
