package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReadmeFunction_Run(t *testing.T) {
	f := &ReadmeFunction{}
	errs := f.run("testdata/readme_dir_not_found")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "no such file")
	}

	f = &ReadmeFunction{}
	errs = f.run("testdata/readme_not_found")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "not found")
	}

	f = &ReadmeFunction{}
	errs = f.run("testdata/readme_lower")
	assert.Zero(t, len(errs))

	f = &ReadmeFunction{}
	errs = f.run("testdata/readme_upper")
	assert.Zero(t, len(errs))
}
