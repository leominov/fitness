package main

import (
	"fmt"
	"os"
	"path"
	"regexp"
)

var (
	npmrcAccessTokenRE = regexp.MustCompile(`_authToken=[\w]+`)
)

type NpmrcAccessTokenFunction struct {
	docs string
}

func init() {
	f := &NpmrcAccessTokenFunction{
		docs: "https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/3292725258/npmrc-access-token",
	}
	RegisterFunction("npmrc-access-token", f)
}

func (n *NpmrcAccessTokenFunction) run(dirs []string) (errs []error) {
	for _, dir := range dirs {
		filename := path.Join(dir, ".npmrc")
		b, err := os.ReadFile(path.Join(dir, ".npmrc"))
		if err != nil {
			continue
		}
		if npmrcAccessTokenRE.Match(b) {
			err := fmt.Errorf("access token found in %s file", filename)
			errs = append(errs, NewErrFunctionFailed(err))
		}
	}
	return
}

func (n *NpmrcAccessTokenFunction) Run(_ map[string]interface{}) []error {
	return n.run([]string{".", "web/", "server"})
}

func (n *NpmrcAccessTokenFunction) DocsLink() string {
	return n.docs
}
