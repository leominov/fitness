package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGemfileAccessTokenFunction_Run(t *testing.T) {
	g := &GemfileAccessTokenFunction{}
	errs := g.run("testdata/gemfile-access-token/valid.lock")
	assert.Nil(t, errs)
	errs = g.run("testdata/gemfile-access-token/valid-internal.lock")
	assert.Nil(t, errs)
	errs = g.run("testdata/gemfile-access-token/not-found.lock")
	assert.Nil(t, errs)
	errs = g.run("testdata/gemfile-access-token/invalid.lock")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "access token")
	}
}
