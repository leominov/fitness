package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
)

var (
	lint          = flag.Bool("lint", false, "Check configuration and exit.")
	logLevel      = flag.String("log-level", "info", "Level of logging.")
	alterConfig   = flag.String("config", "", "Path to additional configuration file.")
	report        = flag.Bool("report", true, "Enable reporting to Pushgateway.")
	reportingAddr = flag.String("report-addr", "https://pushgateway.infra.cloud.qlean.ru", "Pushgateway address for reporting.")
)

func main() {
	flag.Parse()

	reporter := NewReporter(*reportingAddr, os.Getenv("CI_PROJECT_PATH"))
	if level, err := logrus.ParseLevel(*logLevel); err == nil {
		logrus.SetLevel(level)
	}

	logrus.SetFormatter(&logrus.TextFormatter{
		ForceColors: true,
	})

	for _, src := range []string{
		"/etc/fitness.d/",
		"fitness.d",
	} {
		LoadFunctionsFromDirectory(src)
	}

	for _, src := range []string{
		"/etc/fitness/fitness.yaml",
		".fitness.yaml",
		*alterConfig,
	} {
		if len(src) == 0 {
			continue
		}
		userConfig, err := LoadFromFile(src)
		if err != nil {
			continue
		}
		config.Merge(userConfig)
	}

	if *lint {
		fmt.Println(config.String())
		return
	}

	var failed bool
	for name, fu := range FunctionsRegistry {
		config, ok := config.Functions[name]
		if !ok {
			logrus.Debugf("s %s", name)
			continue
		}

		if !config.IsEnabled() {
			logrus.Debugf("- %s", name)
			continue
		}

		err := fu.Run(config.Meta)
		if err == nil {
			reporter.SetStatus(name, SuccessFunctionStatus)
			logrus.Infof("✓ %s", name)
			continue
		}

		if config.Threshold.IsEmpty() {
			failed = true
			reporter.SetStatus(name, ErrorFunctionStatus)
			logrus.Errorf("✗ %s: %v", name, err)
		} else {
			msg := fmt.Sprintf("✗ %s (%s): %v", name, config.Threshold.String(), err)
			if config.Threshold.IsError() {
				failed = true
				reporter.SetStatus(name, ErrorFunctionStatus)
				logrus.Error(msg)
			} else if config.Threshold.IsWarn() {
				reporter.SetStatus(name, WarningFunctionStatus)
				logrus.Warn(msg)
			}
		}

		if fu.DocsLink() != "" && (config.Threshold.IsEmpty() || config.Threshold.IsError() || config.Threshold.IsWarn()) {
			logrus.Infof("  Documentation: %s", fu.DocsLink())
		}
	}

	if *report {
		reporter.Send()
	}

	if failed {
		os.Exit(1)
	}
}
