package main

import (
	"fmt"
	"os"
	"strings"
)

type ReadmeFunction struct {
	docs string
}

func init() {
	f := &ReadmeFunction{
		docs: "https://qleanru.atlassian.net/wiki/spaces/DOCS/pages/3291742211/readme",
	}
	RegisterFunction("readme", f)
}

func (r *ReadmeFunction) Run(_ map[string]interface{}) []error {
	return r.run("./")
}

func (r *ReadmeFunction) DocsLink() string {
	return r.docs
}

func (r *ReadmeFunction) run(workDir string) []error {
	var match bool
	entries, err := os.ReadDir(workDir)
	if err != nil {
		return []error{NewErrFunctionFailed(err)}
	}
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}
		if strings.ToLower(entry.Name()) == "readme.md" {
			match = true
			break
		}
	}
	if !match {
		err = fmt.Errorf("README.md not found")
		return []error{NewErrFunctionFailed(err)}
	}
	return nil
}
