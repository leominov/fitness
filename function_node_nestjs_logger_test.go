package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNodeNestJSLoggerFunction_Run(t *testing.T) {
	f := &NodeNestJSLoggerFunction{}
	errs := f.run("testdata/node-nestjs-logger/not-found.json")
	assert.Zero(t, len(errs))

	errs = f.run("testdata/node-nestjs-logger/invalid-not-found.json")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "not found")
	}

	errs = f.run("testdata/node-nestjs-logger/invalid-version.json")
	if assert.Equal(t, 1, len(errs)) {
		err := errs[0]
		assert.Contains(t, err.Error(), "version must be greater")
	}

	errs = f.run("testdata/node-nestjs-logger/valid.json")
	assert.Zero(t, len(errs))

	errs = f.run("testdata/node-nestjs-logger/valid-skip.json")
	assert.Zero(t, len(errs))
}
