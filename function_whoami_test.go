package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExecFunction(t *testing.T) {
	f, ok := FunctionsRegistry["whoami"]
	if assert.True(t, ok) {
		errs := f.Run(nil)
		assert.Nil(t, errs)
	}
}
