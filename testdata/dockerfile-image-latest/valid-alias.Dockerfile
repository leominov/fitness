#--- base stage ----------------------------------------------------------------
FROM node:14-alpine AS base

RUN apk add --no-cache tzdata bash

ENV APP_PATH="/opt/app"

WORKDIR $APP_PATH

#--- build application stage ---------------------------------------------------
FROM base AS build-application

ARG NPM_TOKEN

RUN [ ! -z "${NPM_TOKEN}" ] || { echo "Build-time variable NPM_TOKEN is not set"; exit 1; }

RUN apk add --no-cache \
  python3 \
  make \
  g++

COPY ./package*.json ./

RUN set -xe \
  && echo -e "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > ~/.npmrc \
  && npm ci

COPY . .

RUN npm run prebuild \
  && npm run build

#--- release stage -------------------------------------------------------------
FROM base AS release

COPY --chown=node:node --from=build-application $APP_PATH/node_modules ./node_modules
COPY --chown=node:node --from=build-application $APP_PATH/dist ./
# COPY --chown=node:node --from=build-application $APP_PATH/proto ./proto
COPY --chown=node:node package.json ./
COPY --chown=node:node docker-entrypoint.sh ./

USER node

ENV PATH="$PATH:$APP_PATH/node_modules/.bin"

ENTRYPOINT ["./docker-entrypoint.sh"]
