FROM golang:1.16-stretch as build
WORKDIR /go/src/gitlab.qleanlabs.ru/platform/infra/fitness/
COPY . ./
RUN go build .

FROM debian:10-slim as release
COPY .fitness.default.yaml /etc/fitness/fitness.yaml
COPY fitness.d /etc/fitness.d
RUN apt-get update \
    && apt-get install -y ca-certificates \
    && apt-get autoremove -y \
    && apt-get autoclean \
    && apt-get clean
COPY --from=build /go/src/gitlab.qleanlabs.ru/platform/infra/fitness/fitness /usr/local/bin/fitness
ENTRYPOINT ["fitness"]
