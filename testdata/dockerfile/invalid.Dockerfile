FROM golang:1.16-stretch as build
WORKDIR /go/src/gitlab.qleanlabs.ru/platform/infra/fitness/
COPY . ./
ENV foobar
RUN go build .
