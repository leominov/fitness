module gitlab.qleanlabs.ru/platform/infra/fitness

go 1.16

require (
    github.com/Masterminds/semver v1.5.0
    github.com/moby/buildkit v0.9.0
    github.com/sirupsen/logrus v1.8.1
    github.com/stretchr/testify v1.7.0
    gopkg.in/yaml.v2 v2.4.0
)
