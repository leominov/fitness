module gitlab.qleanlabs.ru/platform/go-libs/service-loyalty-bonus-sdk

go 1.15

require (
    github.com/davecgh/go-spew v1.1.1 // indirect
    github.com/golang/protobuf v1.4.3
    github.com/kr/pretty v0.1.0 // indirect
    github.com/pkg/errors v0.9.1
    gitlab.qleanlabs.ru/platform/go-libs/base-sdk v1.0.2
    golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
    golang.org/x/sys v0.0.0-20190826190057-c7b8b68b1456 // indirect
    golang.org/x/text v0.3.3 // indirect
    google.golang.org/grpc v1.34.0
    google.golang.org/protobuf v1.25.0
    gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
