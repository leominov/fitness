package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsValuesFileContainsOwnerTeam(t *testing.T) {
	_, err := IsValuesFileContainsOwnerTeam("testdata/helm-app-owner-team/not-found.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "no such file")
	}

	ok, err := IsValuesFileContainsOwnerTeam("testdata/helm-app-owner-team/valid-owner-team.yaml")
	if assert.NoError(t, err) {
		assert.True(t, ok)
	}

	ok, err = IsValuesFileContainsOwnerTeam("testdata/helm-app-owner-team/missed-owner-team.yaml")
	if assert.NoError(t, err) {
		assert.False(t, ok)
	}

	ok, err = IsValuesFileContainsOwnerTeam("testdata/helm-app-owner-team/invalid-syntax.yaml")
	if assert.Error(t, err) {
		assert.Contains(t, err.Error(), "yaml")
	}
}

func TestHelmAppOwnerTeamFunction_Run(t *testing.T) {
	f := &HelmAppOwnerTeamFunction{}
	errs := f.run("testdata/helm-app-owner-team/not_found_dir")
	assert.Nil(t, errs)
	errs = f.run("testdata/helm-app-owner-team/valid_common_values")
	assert.Nil(t, errs)
	errs = f.run("testdata/helm-app-owner-team/empty_common_values")
	if assert.Equal(t, 1, len(errs)) {
		assert.Contains(t, errs[0].Error(), "empty_common_values/stage.values.yaml")
	}
	errs = f.run("testdata/helm-app-owner-team/invalid_syntax_values")
	if assert.Equal(t, 1, len(errs)) {
		assert.Contains(t, errs[0].Error(), "yaml")
	}
}
